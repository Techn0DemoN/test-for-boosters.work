<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Company;

class CompanyController extends FOSRestController{
    
    /**
     * @Rest\Get("/api/companies")
     */
    public function getCompanies()
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Company')->findAll();
        if ($restresult === null) {
            $response['errors']['message'] = 'There are no company exist';
            return new View($response, Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }
    
    /**
     * @Rest\Post("/api/companies")
     */
    public function createCompany(Request $request)
    {
        $country = $this->getDoctrine()->getRepository('AppBundle:Country')->findOneById((int)$request->get('countryId'));
        if (empty($country)) {
            $response['errors']['id'] = 'Country not found';
            return new View($response, Response::HTTP_NOT_FOUND);
        }
        
        $company = new Company;
        $company->setName($request->get('name'));
        $company->setEmail($request->get('email'));
        $company->setCountryId($country);
        $validator = $this->get('validator');
        $errors = $validator->validate($company);
        if(count($errors) > 0) {
            return new View($errors, Response::HTTP_NOT_ACCEPTABLE);
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($company);
        $em->flush();
        return new View($company, Response::HTTP_OK);
    }    
    
    /**
    * @Rest\Put("/api/companies/{id}", requirements={"id" = "\d+"})
    */
    public function updateCompany($id, Request $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $countryId = (int)$request->get('countryId');
        
        $country = $this->getDoctrine()->getRepository('AppBundle:Country')->find($countryId);
        if (empty($country)) {
            $response['errors']['country_id'] = 'Country not found';
            return new View($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }   
            
        $company = $this->getDoctrine()->getRepository('AppBundle:Company')->find($id);
        if (empty($company)) {
            $response['errors']['id'] = 'Not found';
            return new View($response, Response::HTTP_NOT_FOUND);
        } 
        
        $company->setName($name);
        $company->setEmail($email);
        $company->setCountryId($country);
        
        $validator = $this->get('validator');
        $errors = $validator->validate($company);
        if(count($errors) > 0) {
            return new View($errors, Response::HTTP_NOT_ACCEPTABLE);
        }
  
        $en = $this->getDoctrine()->getManager();
        $en->flush();
        return new View($company, Response::HTTP_OK);
    }    
    
    /**
    * @Rest\Delete("/api/companies/{id}", requirements={"id" = "\d+"})
    */
    public function deleteCompanies($id)
    {
        $em = $this->getDoctrine()->getManager();
        $company = $this->getDoctrine()->getRepository('AppBundle:Company')->find($id);
        if (empty($company)) {
            $response['errors']['message'] = 'Company not found';
            return new View($response, Response::HTTP_NOT_FOUND);
        }
        $em->remove($company);
        $em->flush();
        $response['message'] = 'Deleted';
        return new View($response, Response::HTTP_OK);
    }
    
}
