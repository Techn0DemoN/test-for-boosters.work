<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

class ReportController extends FOSRestController{
       
    /**
     * @Rest\Post("/api/generate")
     */
    public function generateData()
    {
        $this->get('app.general')->generateData();
        $response['message'] = 'Generated';
        return new View($response, Response::HTTP_OK);
    }
    
    /**
     * @Rest\Get("/api/report/{year}/{month}", requirements={"year" = "\d+", "month" = "\d+"})
     */
    public function generateReport($year, $month)
    {
        $report['country'] = $this->get('app.general')->generateReportCountry($year, $month);
        $report['company'] = $this->get('app.general')->generateReportCompany($year, $month);
        return new View($report, Response::HTTP_OK);
    }
    

}