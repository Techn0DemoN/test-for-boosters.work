<?php

namespace AppBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Country;

class CountryController extends FOSRestController{
    
    /**
     * @Rest\Get("/api/countries")
     */
    public function getCountries()
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Country')->findAll();
        if ($restresult === null) {
            $response['errors']['message'] = 'There are no countries exist';
            return new View($response, Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }
    
    /**
     * @Rest\Post("/api/country")
     */
    public function createCountry(Request $request)
    {
        $country = new Country;
        $country->setName($request->get('name'));
        $country->setPlan((int)$request->get('plan'));
        
        $validator = $this->get('validator');
        $errors = $validator->validate($country);
        if (count($errors) > 0) {
            return new View($errors, Response::HTTP_NOT_ACCEPTABLE);
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($country);
        $em->flush();
        return new View($country, Response::HTTP_CREATED);
    }

    /**
    * @Rest\Put("/api/country/{id}", requirements={"id" = "\d+"})
    */
    public function updateCountry($id, Request $request)
    {
        $name = $request->get('name');
        $plan = (int)$request->get('plan');
        $en = $this->getDoctrine()->getManager();
        
        $country = $this->getDoctrine()->getRepository('AppBundle:Country')->find($id);
        if (empty($country)) {
            $response['errors']['id'] = 'Not found';
            return new View($response, Response::HTTP_NOT_FOUND);
        }
        
        $country->setName($name);
        $country->setPlan($plan);
            
        $validator = $this->get('validator');
        $errors = $validator->validate($country);
        if (count($errors) > 0) {
            return new View($errors, Response::HTTP_NOT_ACCEPTABLE);
        }
        $en->flush();
        return new View($country, Response::HTTP_OK);
    }
    
    /**
    * @Rest\Delete("/api/country/{id}")
    */
    public function deleteCountry($id)
    {
        $em = $this->getDoctrine()->getManager();
        $country = $this->getDoctrine()->getRepository('AppBundle:Country')->find($id);
        if (empty($country)) {
            $response['errors']['id'] = 'Country not found';
            return new View($response, Response::HTTP_NOT_FOUND);
        }
        else {
            $em->remove($country);
            $em->flush();
        }
        $response['message'] = 'Deleted';
        return new View($response, Response::HTTP_OK);
    }
    
}
