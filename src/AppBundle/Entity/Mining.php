<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="mining")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 */
class Mining
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Company", inversedBy="company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */    
    private $companyId;    

    /**
     * @var int
     *
     * @ORM\Column(name="datatime", type="integer")
     */
    private $dataTime;

    /**
     * @var int
     *
     * @ORM\Column(name="mined", type="integer")
     */
    private $mined;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataTime
     *
     * @param integer $dataTime
     *
     * @return Mining
     */
    public function setDataTime($dataTime)
    {
        $this->dataTime = $dataTime;

        return $this;
    }

    /**
     * Get dataTime
     *
     * @return integer
     */
    public function getDataTime()
    {
        return $this->dataTime;
    }

    /**
     * Set mined
     *
     * @param integer $mined
     *
     * @return Mining
     */
    public function setMined($mined)
    {
        $this->mined = $mined;

        return $this;
    }

    /**
     * Get mined
     *
     * @return integer
     */
    public function getMined()
    {
        return $this->mined;
    }

    /**
     * Set companyId
     *
     * @param \AppBundle\Entity\Company $companyId
     *
     * @return Mining
     */
    public function setCompanyId(\AppBundle\Entity\Company $companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }
}
