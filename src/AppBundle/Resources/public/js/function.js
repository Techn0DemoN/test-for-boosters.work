$(document).ready(function () {
    $("#loader-animation").hide();
});

$(document)
    .ajaxStart(function () {
        $("#loader-animation").show();
    })
    .ajaxStop(function () {
        $("#loader-animation").hide();
    });
    
function showMessage(text, type) {
    if (type == null) {
        $(".alert-success").text(text);
        $(".alert-success").show();
        setTimeout(function(){$(".alert-success").hide();}, 2000);
    } else {
        $(".alert-danger").text(text);
        $(".alert-danger").show();
        setTimeout(function(){$(".alert-danger").hide();}, 2000); 
    }
}

    