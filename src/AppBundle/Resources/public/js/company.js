$(document).ready(function() {
    showAllCompanies(); 
    showInputAllCountry();
    // delete company
    $('#company').on('click','.company-del', function(){
        var id = $(this).closest('tr').data('id');
        $.ajax({
            method: "DELETE",
            url: "/api/companies/" + id,
            cache: false,
            dataType: 'json',
            success: function(data) {
                showAllCompanies();
                showMessage('Deleted');
            },
            error: function() {
                console.log('Error');   
            }
        });
    });

    // create company
    $('#companyAddForm').submit(function (e) {
        var data = $(this).serializeArray();
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "/api/companies",
            cache: false,
            dataType: 'json',
            data: data,
            success: function(data) {
                showAllCompanies();
                $('#company-add-show-form').show();
                $('#companyAddForm').hide(); 
                $('#companyAddForm input').val('');
                showMessage('Created');
            },
            error: function() {
                console.log('Error');   
            }
        });
    });  
    
    // edit company (show form)
    $('#company').on('click','.company-edit', function(){
        $(this).closest('tr').find('td:eq(0)').html('<input name="name" type="text" class="form-control" value="' + $(this).closest('tr').find('td:eq(0)').html() + '">');
        $(this).closest('tr').find('td:eq(1)').html('<input name="email" type="text" class="form-control" value="' + $(this).closest('tr').find('td:eq(1)').html() + '">');
        $(this).closest('tr').find('td:eq(2)').html('<select name="countryId" class="form-control companyCountry"></select>');
        $(this).closest('tr').find('td:eq(3)').html('<button type="button" id="company-update" class="btn btn-success">Save</button> <button type="button" id="company-update-cancel" class="btn btn-warning">Cancel</button>');
        showInputAllCountry();
    }); 

    // edit company save
    $('#company').on('click','#company-update', function(){
        $.ajax({
            method: "PUT",
            url: "/api/companies/" + $(this).closest('tr').data('id'),
            cache: false,
            dataType: 'json',
            data: {
                'name': $(this).closest('tr').find('td:eq(0) input').val(),
                'email': $(this).closest('tr').find('td:eq(1) input').val(),
                'countryId': $(this).closest('tr').find('td:eq(2) select').val()
            },
            success: function(data) {
                showAllCompanies();
                showMessage('Saved');
            },
            error: function() {
                console.log('Error');   
            }
        });
    });
    
    // edit company (cancel form)
    $('#company').on('click','#company-update-cancel', function(){
        showAllCompanies();
    });    

    // add company (show form)
    $('#company').on('click','#company-add-show-form', function(){
        $('#company-add-show-form').hide();
        $('#companyAddForm').show();
    });
    
    // add company (hide form)
    $('#company').on('click','#company-add-hide-form', function(){
        $('#company-add-show-form').show();
        $('#companyAddForm').hide();
        $('#companyAddForm input').val('');
    });
});

function showInputAllCountry() {
    $.ajax({
        method: "GET",
        url: "/api/countries",
        cache: false,
        dataType: 'json',
        success: function(data) {
            $('.companyCountry').html('');
            $.each(data, function(i, val) {
                $('.companyCountry').append('<option value="' + val.id + '">' + val.name + '</option>');
            });
        },
        error: function() {
            console.log('Error');   
        }
    });
}

function showAllCompanies() {
    $.ajax({
        method: "GET",
        url: "/api/companies",
        cache: false,
        dataType: 'json',
        success: function(data) {
            $('#company').find('tbody').html('');
            $.each(data, function(i, val) {
                $('#company').find('tbody').append('<tr data-id="' + val.id + '">'
                + '<th scope="row">' + val.id + '</th>'
                + '<td>' + val.name + '</td>'
                + '<td>' + val.email + '</td>'
                + '<td>' + val.country_id.name + '</td>'
                + '<td>'
                + '<button type="button" class="btn btn-warning company-edit">Edit</button> '
                + '<button type="button" class="btn btn-danger company-del">Delete</button>'
                + '</td>'
                + '</tr>');
            });
        },
        error: function() {
            console.log('Error');   
        }
    });
}  