$(document).ready(function() {
    $('#data-container').datepicker({
        format: "mm/yyyy",
        minViewMode: 1,
        autoclose: true,
        defaultDate: new Date(),
        constraintInput:true
    });
    
    // generate data
    $('#leaders').on('click','#generate-data', function(){
        $.ajax({
            method: "POST",
            url: "/api/generate",
            cache: false,
            success: function() {
                showMessage('Data generated');
            },
            error: function() {
                console.log('Error');   
            }
        });
    });
    
    $('#generate-report').click(function() {
        var datepickerDate = $('#data-container').data("datepicker").getDate(),
            fullYear, month;
            
        if (datepickerDate === null){
            showMessage('Select month', 1);
            return;
        } else {
            fullYear = datepickerDate.getFullYear();
            month = datepickerDate.getMonth() + 1;
        }
        
        $.ajax({
            method: "GET",
            url: "/api/report/" + fullYear + "/" + month,
            cache: false,
            dataType: 'json',
            success: function(data) {
                showReport(data);
                showMessage('Report generated');
            },
            error: function() {
                console.log('Error');   
            }
        });
    });
});

function showReport(data) {
    $('#leaders').find('tbody').html('');
    $.each(data.country, function(i, val) {
        $('#leaders').find('#counry-report').append('<tr>'
            + '<td>' + val.name + '</td><td>' + val.mined + '</td><td>' + val.plan + '</td>'
            + '</tr>');
    });
    $.each(data.company, function(i, val) {
        $('#leaders').find('#company-report').append('<tr>'
            + '<td>' + val.name + '</td><td>' + val.mined + '</td><td>' + val.plan + '</td>'
            + '</tr>');
    });
}
   