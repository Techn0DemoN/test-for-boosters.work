$(document).ready(function() {
    showAllCountries();
    
    // delete country
    $('#country').on('click','.country-del', function(){
        var id = $(this).closest('tr').data('id');
        $.ajax({
            method: "DELETE",
            url: "/api/country/" + id,
            cache: false,
            dataType: 'json',
            success: function(data) {
                showAllCountries();
                showAllCompanies();
                showInputAllCountry();
                showMessage('Deleted');
            },
            error: function() {
                console.log('Error');   
            }
        });
    });
    
    // create country
    $('#countryAddForm').submit(function (e) {
        var data = $(this).serializeArray();
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "/api/country",
            cache: false,
            dataType: 'json',
            data: data,
            success: function(data) {
                showAllCountries();
                $('#country-add-show-form').show();
                $('#countryAddForm').hide(); 
                $('#countryAddForm input').val('');
                showInputAllCountry();
                showMessage('Created');
            },
            error: function() {
                console.log('Error');   
            }
        });
    });     

    // edit country (show form)
    $('#country').on('click','.country-edit', function(){
        $(this).closest('tr').find('td:eq(0)').html('<input name="name" type="text" class="form-control" value="' + $(this).closest('tr').find('td:eq(0)').html() + '">');
        $(this).closest('tr').find('td:eq(1)').html('<input name="plan" type="text" class="form-control" value="' + $(this).closest('tr').find('td:eq(1)').html() + '">');
        $(this).closest('tr').find('td:eq(2)').html('<button type="button" id="country-update" class="btn btn-success">Save</button> <button type="button" id="country-update-cancel" class="btn btn-warning">Cancel</button>');
    }); 

    // edit country save
    $('#country').on('click','#country-update', function(){
        $.ajax({
            method: "PUT",
            url: "/api/country/" + $(this).closest('tr').data('id'),
            cache: false,
            dataType: 'json',
            data: {
                'name': $(this).closest('tr').find('td:eq(0) input').val(),
                'plan': $(this).closest('tr').find('td:eq(1) input').val()
            },
            success: function(data) {
                showAllCountries();
                showAllCompanies();
                showMessage('Updated');
            },
            error: function() {
                console.log('Error');   
            }
        });
    });
    
    // edit country (cancel form)
    $('#country').on('click','#country-update-cancel', function(){
        showAllCountries();
    });
    
    // add country (show form)
    $('#country').on('click','#country-add-show-form', function(){
        $('#country-add-show-form').hide();
        $('#countryAddForm').show();
    });
    
    // add country (hide form)
    $('#country').on('click','#country-add-hide-form', function(){
        $('#country-add-show-form').show();
        $('#countryAddForm').hide();
        $('#countryAddForm input').val('');
    });
       
});

function showAllCountries() {
    $.ajax({
        method: "GET",
        url: "/api/countries",
        cache: false,
        dataType: 'json',
        success: function(data) {
            $('#country').find('tbody').html('');
            $.each(data, function(i, val) {
                $('#country').find('tbody').append('<tr data-id="' + val.id + '">'
                + '<th scope="row">' + val.id + '</th>'
                + '<td>' + val.name + '</td>'
                + '<td>' + val.plan + '</td>'
                + '<td>'
                + '<button type="button" class="btn btn-warning country-edit">Edit</button> '
                + '<button type="button" class="btn btn-danger country-del">Delete</button>'
                + '</td>'
                + '</tr>');
            });
        },
        error: function() {
            console.log('Error');   
        }
    });
}   