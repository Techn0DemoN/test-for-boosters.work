<?php

namespace AppBundle\Service;

use Carbon\Carbon;
use AppBundle\Entity\Mining;

class GeneralService
{
    protected $container;
    protected $em;
    protected $connect;
    protected $miningWeightMin = 100;       // минимальный добытый вес за один раз
    protected $miningWeightMax = 125000;    // максимальный добытый вес за 1 раз (125000*80 = 10000000) (то-есть максимально за месяц можно добыть 10 тонн
    protected $miningCountInMonthMin = 1;   // минимальное количество работ в месяц
    protected $miningCountInMonthMax = 80;  // максимальное количество работ в месяц
    
    public function __construct($container)
    {
        $this->container = $container;
        $this->em        = $this->container->get('doctrine.orm.default_entity_manager');
        $this->conn      = $this->em->getConnection();
    }

    public function generateData()
    {
        $allCompanies = $this->container->get('doctrine')->getRepository('AppBundle:Company')->findAll();
        $em = $this->container->get('doctrine')->getManager();

        foreach ($allCompanies as $company) { // перебираем все компании
            $dateStartCarbon = Carbon::now()->startOfMonth();
            $dateEndCarbon  = Carbon::now()->endOfMonth(); 
            $dateEndNowCarbon  = Carbon::now();
            for($month = 0; $month < 7; $month++) { // с текущей даты по идем на месяц назад и так до 6 месяцев
                if ($month == 0) {
                    $dateStart = $dateStartCarbon->timestamp;
                    $dateEnd = $dateEndNowCarbon->timestamp;
                } else {
                    $dateStartCarbon->subMonth(); // переходим на месяц назад
                    $dateEndCarbon->subMonth();   // переходим на месяц назад
                    $dateStart = $dateStartCarbon->timestamp;
                    $dateEnd = $dateEndCarbon->timestamp;
                }
                $miningCountInMonth = mt_rand($this->miningCountInMonthMin, $this->miningCountInMonthMax); // рандомно определяем количество работ в этом месяце
                for($miningCount = 0; $miningCount < $miningCountInMonth; $miningCount++) { // по очереди записываем работы, генерируя к ним данные
                    $miningWeight = mt_rand($this->miningWeightMin, $this->miningWeightMax); // рандомно определяем сколько добыли за раз
                    $date = mt_rand($dateStart, $dateEnd);
                    $mining = new Mining;
                    $mining->setCompanyId($company);
                    $mining->setDataTime($date);
                    $mining->setMined($miningWeight);
                    $em->persist($mining);  
                }
            }
        }
        $em->flush();
    }
    
    public function generateReportCountry($year, $month)
    {
        $date = Carbon::createFromDate($year, $month);
        $dateStart = $date->startOfMonth()->timestamp;
        $dateEnd = $date->endOfMonth()->timestamp;
        $sql = "
            SELECT `country_id`, SUM(`mined`) as `mined`, `cu`.`name`, `cu`.`plan`
            FROM `mining` as `mi`
            LEFT JOIN `company` as `co` ON `co`.`id` = `mi`.`company_id`
            LEFT JOIN `country` as `cu` ON `cu`.`id` = `co`.`country_id`
            WHERE `datatime` >= :dateStart AND `datatime` <= :dateEnd
            GROUP BY `country_id`
            HAVING SUM(`mined`) > `cu`.`plan`
            ORDER BY `mined` DESC
        ";
        
        $result  = $this->query($sql, [":dateStart" => $dateStart, ":dateEnd" => $dateEnd]);
        return $result->fetchAll();
    }
    
    public function generateReportCompany($year, $month)
    {
        $date = Carbon::createFromDate($year, $month);
        $dateStart = $date->startOfMonth()->timestamp;
        $dateEnd = $date->endOfMonth()->timestamp;
        $sql = "
            SELECT `company_id`, SUM(`mined`) as `mined`, `co`.`name`, `cu`.`plan`
            FROM `mining` as `mi`
            LEFT JOIN `company` as `co` ON `co`.`id` = `mi`.`company_id`
            LEFT JOIN `country` as `cu` ON `cu`.`id` = `co`.`country_id`
            WHERE `datatime` >= :dateStart AND `datatime` <= :dateEnd
            GROUP BY `company_id`
            HAVING SUM(`mined`) > `cu`.`plan`
            ORDER BY `cu`.`plan` DESC         
        ";
        
        
        $result  = $this->query($sql, [":dateStart" => $dateStart, ":dateEnd" => $dateEnd]);
        return $result->fetchAll();
    }
    
    protected function query($sql, $data = [])
    {
        $st = $this->conn->prepare($sql);

        foreach ($data as $key => $value) {
            $st->bindValue($key, $value);
        }
        $st->execute();
        return $st;
    }

}