<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171226135605 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        
        $this->addSql('
            INSERT INTO `country` (`id`, `name`, `plan`) VALUES
            (1, \'Canada\', 10000000),
            (2, \'USA\', 1000000),
            (3, \'Russia\', 8000000),
            (4, \'Australia\', 900000);
        ');
        
        $this->addSql('
            INSERT INTO `company` (`id`, `country_id`, `name`, `email`) VALUES
            (1, 1, \'Goldcorp\', \'goldcorp@mail.com\'),
            (2, 1, \'Barrick Gold\', \'barrick@gold.com\'),
            (3, 2, \'Newmont Mining\', \'newmont@mining.com\'),
            (4, 3, \'Polyus Gold\', \'polyus@gold.com\'),
            (5, 4, \'Newcrest Mining\', \'newcrest@mining.com\');
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
